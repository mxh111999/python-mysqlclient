Name:    python-mysqlclient
Version: 2.2.5
Release: 1
Summary: MySQL database connector for Python
License: GPLv2+
URL:     https://github.com/PyMySQL/mysqlclient-python
Source0: https://files.pythonhosted.org/packages/be/95/1af2ee813d4f0b607082c18bb82aa05c98a95a402a1d2d5808999317cb16/mysqlclient-2.2.5.tar.gz

BuildRequires: gcc mariadb-connector-c-devel openssl-devel zlib-devel
BuildRequires: python3-devel python3-setuptools python3-debug

Provides:  python-mysql = %{version}-%{release}
Obsoletes: python-mysql < %{version}-%{release}

%description
MySQLdb is an interface to the popular MySQL database server that
provides the Python database API.

%package -n python3-mysqlclient
Summary:   MySQL database connector for Python3
Provides:  MySQL-python3 = %{version}-%{release}
Obsoletes: MySQL-python3 < %{version}-%{release}
Provides:  python3-mysql = %{version}-%{release}
Obsoletes: python3-mysql < %{version}-%{release}
Provides:  python3-mysql%{_isa} = %{version}-%{release}

%description -n python3-mysqlclient
MySQL database connector for Python3

%package -n python3-mysqlclient-debug
Summary:   Python3 interface to MySQL, built for the CPython debug runtime
Requires:  python3-mysqlclient%{_isa} = %{version}-%{release}
Provides:  MySQL-python3-debug = %{version}-%{release}
Obsoletes: MySQL-python3-debug < %{version}-%{release}
Provides:  python3-mysql-debug = %{version}-%{release}
Obsoletes: python3-mysql-debug < %{version}-%{release}
Provides:  python3-mysql-debug%{_isa} = %{version}-%{release}

%description -n python3-mysqlclient-debug
Python3 interface to MySQL, built for the CPython debug runtime


%prep
%autosetup -n mysqlclient-%{version} -p1

%build
%py3_build
CFLAGS="$RPM_OPT_FLAGS" %{__python3}-debug setup.py build


%install
%py3_install
%{__python3}-debug setup.py install -O1 --root %{buildroot} --skip-build


%files -n python3-mysqlclient
%doc README.md doc/*.rst
%license LICENSE
%dir %{python3_sitearch}/MySQLdb
%{python3_sitearch}/MySQLdb/*.py
%{python3_sitearch}/MySQLdb/_mysql.cpython-3?*.so
%{python3_sitearch}/MySQLdb/_mysql.c
%dir %{python3_sitearch}/MySQLdb/__pycache__
%{python3_sitearch}/MySQLdb/__pycache__/*
%dir %{python3_sitearch}/MySQLdb/constants
%{python3_sitearch}/MySQLdb/constants/*.py
%dir %{python3_sitearch}/MySQLdb/constants/__pycache__
%{python3_sitearch}/MySQLdb/constants/__pycache__/*
%dir %{python3_sitearch}/mysqlclient-%{version}-py3.*.egg-info
%{python3_sitearch}/mysqlclient-%{version}-py3.*.egg-info/*

%files -n python3-mysqlclient-debug
%{python3_sitearch}/MySQLdb/_mysql.cpython-3*d*.so

%changelog
* Wed Oct 30 2024 muxiaohui <muxiaohui@kylinos.cn> - 2.2.5-1
- Update version to 2.2.5
  Update license metadata.
  Support building against Percona Server builds of MySQL client library.

* Tue Aug 13 2024 Ge Wang <wang__ge@126.com> - 2.2.4-1
- Upgrade to 2.2.4

* Wed Aug 03 2022 kkz <zhaoshuang@uniontech.com> - 2.1.1-1
- Upgrade to 2.1.1

* Thu May 19 2022 yanglongkang <yanglongkang@h-partners.com> - 1.3.12-10
- update the spec file to adapt python3.10

* Fri Oct 30 2020 yanglongkang <yanglongkang@huawei.com> - 1.3.12-9
- remove python2 dependency

* Thu Jun 18 2020 jinzhimin<jinzhimin2@huawei.com> - 1.3.12-8
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:Adapt to the new features of python 3.8

* Fri Feb 14 2020 hy-euler <eulerstoragemt@huawei.com> - 1.3.12-7
- Package Initialization
